using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    public Rigidbody2D myRb;
    private Animator myAni;

    public float forcex;
    public float forcey;

    [SerializeField]
    private AudioSource audioSource;

    [SerializeField]
    private AudioClip flyClip, pingClip, dieClip;

    public GameObject GameOver;

    public bool check;
    // Start is called before the first frame update
    void Start()
    {
        myRb = GetComponent<Rigidbody2D>();
        myAni = GetComponent<Animator>();
        check = true;
        
    }

    // Update is called once per frame
    void Update()
    {
        if(check)
        {
            CharacterMove();
        }
        if(ScorceManager.instance.score==3)
        {
            check = false;
            myRb.gravityScale = 0;
        }    
        if(transform.position.y<=-6f)
        {
            Die();
        }    
    }

    // di chuyen
    public void CharacterMove()
    {
        if(Input.GetMouseButtonDown(0))
        {
            Fly();
        }  
    }
    public void Fly()
    {
        myRb.velocity = new Vector2(forcex, forcey);
        audioSource.PlayOneShot(flyClip);
    }    

    // va cham
    private void OnCollisionEnter2D(Collision2D other)
    {
        FlipFace();

        // va cham voi chuong ngai vat
        if(other.gameObject.tag == "enemy")
        {
            Die();
            var angle = Mathf.Lerp(0, 90, myRb.velocity.y / 7);
            transform.rotation = Quaternion.Euler(0, 0, -angle);

        }
    }
    // va cham voi cherry

    private void OnTriggerEnter2D(Collider2D other)
    {
        audioSource.PlayOneShot(pingClip);
    }
    // xoay mat cua bird
    public void FlipFace()
    {
        // thay doi luc di chuyen
        forcex = -forcex;
        Fly();

        // xoay mat
        var theScale = transform.localScale.x;
        theScale = -theScale;
        transform.localScale = new Vector3(theScale, transform.localScale.y, transform.localScale.z);
    }

    // khi player chet
    public void Die()
    {
        check = false;
        myAni.SetInteger("status", 1);
        audioSource.PlayOneShot(dieClip);
        GameOver.SetActive(true);
    }    
}
