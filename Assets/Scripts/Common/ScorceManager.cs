using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class ScorceManager : MonoBehaviour
{
    public GameObject completeLevel;

    public static ScorceManager instance;
    public TextMeshProUGUI text;

    public int score=0;

    void Start()
    {
        instance = this;
    }

    private void Update()
    {
        CompleteLevel();
    }
    public void ChangeScore()
    {
        score += 1;
        text.text = "X" + score.ToString();
    }

    // hoan thanh level khi an du 3 cherry
    public void CompleteLevel()
    {
        if(score==3)
        {
            completeLevel.SetActive(true);
            Pass();
        }
    }
    public void Pass()
    {
        int currentLevel = SceneManager.GetActiveScene().buildIndex;

        if(currentLevel>=PlayerPrefs.GetInt("levelsUnlock"))
        {
            PlayerPrefs.SetInt("levelsUnlock", currentLevel + 1);
        }
    }
}
