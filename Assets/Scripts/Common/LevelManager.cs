using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    int levelsUnlock;
    public Button[] buttons;


    void Start()
    {
        levelsUnlock = PlayerPrefs.GetInt("levelsUnlock",1);

        for(int i=0;i<buttons.Length;i++)
        {
            buttons[i].interactable = false;
        }
        for (int i = 0; i < levelsUnlock; i++)
        {
            buttons[i].interactable = true;
        }
    }
    public void LoadLevel(int indexLevel)
    {
        SceneManager.LoadScene(indexLevel);
    }
}
