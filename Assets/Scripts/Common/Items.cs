using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Items : MonoBehaviour
{
    [SerializeField]
    private GameObject effectHit;



    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            ScorceManager.instance.ChangeScore();
            Destroy(gameObject);
            var effect = Instantiate(effectHit,transform.position,transform.rotation);
            Destroy(effect, 1f);
        }    
    }
}
