using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float speed;
    bool check;

    // Update is called once per frame
    void Update()
    {
        if (transform.position.y >= 1)
        {
            check = true;
        }
        if (transform.position.y <= -1.5f)
        {
            check = false;
        }
        Movement();
    }
    public void Movement()
    {
        if(check==true)
        {
            transform.Translate(Vector3.down * speed * Time.deltaTime);
        }
        else
        {
            transform.Translate(Vector3.up * speed * Time.deltaTime);
        }    
    }    
}
